package inmemory

import (
	"fmt"
	"sync"
)

type storage struct {
	data map[string]string
	mu   sync.RWMutex
}

func NewStorage() *storage {
	return &storage{
		data: make(map[string]string),
	}
}

func (s *storage) Get(key string) (string, error) {

	defer func() {
		if msg := recover(); msg != nil {
			fmt.Println(msg)
		}
	}()

	s.mu.RLock()
	result, ok := s.data[key]
	s.mu.RUnlock()

	if !ok {
		return "", fmt.Errorf("storage, can not get value for key %v", key)
	}

	return result, nil
}

func (s *storage) Set(key, value string) error {
	defer func() {
		if msg := recover(); msg != nil {
			fmt.Println(msg)
		}
	}()

	s.mu.Lock()
	s.data[key] = value
	s.mu.Unlock()

	return nil
}

func (s *storage) Delete(key string) error {
	defer func() {
		if msg := recover(); msg != nil {
			fmt.Println(msg)
		}
	}()

	s.mu.Lock()
	delete(s.data, key)
	s.mu.Unlock()

	return nil
}

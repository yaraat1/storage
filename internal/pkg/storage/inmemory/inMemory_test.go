package inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetData(t *testing.T) {
	stor := NewStorage()

	err := stor.Set("testKey", "testValue")

	assert.Nil(t, err)

	value, ok := stor.data["testKey"]

	assert.Equal(t, ok, true)

	assert.Equal(t, value, "testValue")

}

func TestGetData(t *testing.T) {
	stor := NewStorage()

	err := stor.Set("testKey", "testValue")

	assert.Nil(t, err)

	value, err := stor.Get("testKey")

	assert.Nil(t, err)

	assert.Equal(t, value, "testValue")

}

func TestDeletData(t *testing.T) {
	stor := NewStorage()

	err := stor.Set("testKey", "testValue")

	assert.Nil(t, err)

	_, err = stor.Get("testKey")

	assert.Nil(t, err)

	err = stor.Delete("testKey")

	assert.Nil(t, err)

	value, ok := stor.data["testKey"]

	assert.Equal(t, ok, false)

	assert.Equal(t, value, "")

}

package memcached

import (
	"fmt"
	"net"
	"strconv"
	"strings"
	"sync"
)

type Client struct {
	address           string
	port              int
	conns             chan net.Conn
	maxBytesPerRecord int64
	waitClose         sync.RWMutex
}

func NewClient(address string, port int, maxConn int64) (*Client, error) {
	if maxConn <= 0 {
		maxConn = 10
	}

	client := &Client{
		conns:   make(chan net.Conn, maxConn),
		address: address,
		port:    port,
	}

	err := client.openConns(maxConn)
	if err != nil {
		return nil, err
	}

	stats, err := client.GetStats()
	if err != nil {
		return nil, err
	}

	limit, ok := stats["limit_maxbytes"]
	if ok {
		client.maxBytesPerRecord, err = strconv.ParseInt(limit, 10, 64)
		if err != nil {
			client.maxBytesPerRecord = 0
		}

	}

	return client, nil
}

func (cl *Client) openConns(connsCount int64) error {
	for i := 0; i < int(connsCount); i++ {
		conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", cl.address, cl.port))
		if err != nil {
			return err
		}
		cl.conns <- conn
	}
	return nil
}

func (cl *Client) Close() error {
	cl.waitClose.Lock()
	defer cl.waitClose.Unlock()

	close(cl.conns)

	for conn := range cl.conns {
		conn.Close()
	}

	return nil
}

func (cl *Client) getConn() (net.Conn, error) {
	cl.waitClose.RLock()
	defer cl.waitClose.RUnlock()
	conn := <-cl.conns
	return conn, nil
}

func (cl *Client) runCmd(cmd string) (string, error) {
	conn, _ := cl.getConn()

	_, err := conn.Write([]byte(cmd + "\r\n"))
	if err != nil {
		return "", err
	}

	buffSize := cl.maxBytesPerRecord

	if buffSize == 0 {
		buffSize = 1024 * 5
	}
	var msg string
	buff := make([]byte, buffSize)
	i, err := conn.Read(buff)
	if err != nil {
		return "", nil
	}

	msg = string(buff[:i])

	if msg == "ERROR\r\n" {
		return "", fmt.Errorf("errrrrrrrrr")
	}

	return msg, nil

}

func (cl *Client) GetStats() (map[string]string, error) {
	resp, err := cl.runCmd("stats")
	if err != nil {
		return nil, err
	}
	stats := strings.Split(resp, "\r\n")

	statsMap := make(map[string]string, len(stats))
	for _, value := range stats {
		tmpValue := strings.Split(value, " ")
		if len(tmpValue) < 2 {
			continue
		}
		key := tmpValue[1]
		value := ""
		if len(tmpValue) == 3 {
			value = tmpValue[2]
		}

		statsMap[key] = value

	}

	return statsMap, nil
}

func (cl *Client) Get(key string) (string, error) {
	cmd := fmt.Sprintf("get %s", key)
	resp, err := cl.runCmd(cmd)
	if err != nil {
		return "", nil
	}

	parseResp := strings.Split(resp, "\r\n")
	if len(parseResp) != 3 {
		return "", fmt.Errorf("blalbalblalbal")
	}

	return parseResp[1], nil

}

func (cl *Client) Set(key string, value string) error {
	flags := 0
	exptime := 900
	bytesCount := len(value)

	if int64(bytesCount) > cl.maxBytesPerRecord {
		return fmt.Errorf("value biggest the maxBytesValue")
	}

	cmd := fmt.Sprintf("set %s %d %d %d\r\n%s", key, flags, exptime, bytesCount, value)

	status, err := cl.runCmd(cmd)
	if err != nil {
		return err
	}

	if status != "STORED\r\n" {
		return fmt.Errorf("not stored '" + status + "'")
	}

	return nil
}

func (cl *Client) Delete(key string) error {
	return nil
}

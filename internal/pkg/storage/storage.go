package storage

import (
	"fmt"
)

type BackendStorage interface {
	Get(key string) (string, error)
	Set(key, value string) error
	Delete(key string) error
}

type storage struct {
	st []BackendStorage
}

func (s *storage) Get(key string) (string, error) {
	for _, s := range s.st {
		value, err := s.Get(key)
		if err == nil {
			return value, nil
		}
	}

	return "", fmt.Errorf("backends not ready")
}

func (s *storage) Set(key, value string) error {
	for _, s := range s.st {
		err := s.Set(key, value)
		if err == nil {
			return nil
		}
	}
	return nil
}

func (s *storage) Delete(key string) error {
	for _, s := range s.st {
		err := s.Delete(key)
		if err == nil {
			return nil
		}
	}
	return nil
}

func NewStorage(st ...BackendStorage) *storage {
	return &storage{
		st: st,
	}

}

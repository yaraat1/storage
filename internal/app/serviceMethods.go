package app

import (
	"context"
	"fmt"

	st "gitlab.com/yaraat1/storage/pkg/storage_service"
)

func (s *StorageServiceServer) GetItem(ctx context.Context, request *st.GetItemRequest) (*st.GetItemResponse, error) {
	val, err := s.storageService.Get(request.Key)
	if err != nil {
		fmt.Printf("errog get item - %v", err)
	}

	return &st.GetItemResponse{Response: fmt.Sprintf("%v", val)}, nil
}

func (s *StorageServiceServer) AddItem(ctx context.Context, request *st.AddItemRequest) (*st.AddItemResponse, error) {
	err := s.storageService.Set(request.Key, request.Value)
	if err != nil {
		fmt.Printf("errog add item - %v", err)
	}

	return &st.AddItemResponse{Response: fmt.Sprintf("ok")}, nil

}

func (s *StorageServiceServer) DeleteItem(ctx context.Context, request *st.DeleteItemRequest) (*st.DeleteItemResponse, error) {
	err := s.storageService.Delete(request.Key)
	if err != nil {
		fmt.Printf("errog delete item - %v", err)
	}

	return &st.DeleteItemResponse{Response: fmt.Sprintf("ok")}, nil
}

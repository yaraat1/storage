package app

import (
	strg "gitlab.com/yaraat1/storage/internal/pkg/storage"
	st "gitlab.com/yaraat1/storage/pkg/storage_service"
)

type StorageServiceServer struct {
	st.UnimplementedStorageServiceServer
	storageService strg.BackendStorage

	// mu sync.Mutex // protects routeNotes
	// routeNotes map[string][]*pb.RouteNote
}

func NewStorageServiceServer(storageService strg.BackendStorage) *StorageServiceServer {
	return &StorageServiceServer{
		storageService: storageService,
	}
}

// этот main файл нужен был для отладки работы с memchached
package main

import (
	"fmt"

	mem "gitlab.com/yaraat1/storage/internal/pkg/storage/memcached"
)

const (
	addr = "127.0.0.1"
	port = 11211
)

func main() {
	fmt.Println("memcached client start")

	client, err := mem.NewClient(addr, port, 10)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("client created")

	err = client.Set("flag114", "flafalkdfjlakjfladjf")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("value setted")

	resp, err := client.Get("flag114")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(resp)

}

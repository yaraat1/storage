package main

import (
	"flag"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	"gitlab.com/yaraat1/storage/internal/app"
	"gitlab.com/yaraat1/storage/internal/pkg/storage"
	"gitlab.com/yaraat1/storage/internal/pkg/storage/inmemory"
	"gitlab.com/yaraat1/storage/internal/pkg/storage/memcached"
	ss "gitlab.com/yaraat1/storage/pkg/storage_service"
)

var port = 50051
var portMemcached = flag.Int("port", 11211, "The server port")
var addr = flag.String("addr", "localhost", "The server addr")

func main() {
	flag.Parse()
	fmt.Println("start project")

	inMemory := inmemory.NewStorage()
	memcache, err := memcached.NewClient(*addr, *portMemcached, 0)
	if err != nil {
		log.Fatal(err)
	}

	storage := storage.NewStorage(inMemory, memcache)

	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", port))

	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	srv := app.NewStorageServiceServer(storage)

	var opts []grpc.ServerOption

	grpcServer := grpc.NewServer(opts...)
	ss.RegisterStorageServiceServer(grpcServer, srv)
	grpcServer.Serve(lis)

}
